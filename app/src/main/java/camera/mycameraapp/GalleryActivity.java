package camera.mycameraapp;


import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;

import java.io.File;
import java.util.ArrayList;


public class GalleryActivity extends FragmentActivity implements PagerFragmentDetail.OnFragmentInteractionListener {

    private ViewPager mPager;
    private ViewPagerAdapter mPagerAdapter;
    private File[] allFiles;
    private ArrayList<Fragment> fragments;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
    fragments = new ArrayList<Fragment>();

        /***Lista plików z lokalizacji w której zdjęcia zostaja zapisane w aktywności kamery ****/

        File sdCard = Environment.getExternalStorageDirectory();
        File dir = new File(sdCard.getAbsolutePath() + "/camtest");
        allFiles = dir.listFiles();

        /*** Lista fragmwntów - każdy z nich zawiera imageView***/
        for(int i=0; i< allFiles.length;i++)
        {
            fragments.add(new PagerFragmentDetail().newInstance(allFiles[i].getPath()));
        }


        /*** ***/
       FragmentManager manager = getSupportFragmentManager();
        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new ViewPagerAdapter(manager,fragments);
        mPager.setAdapter(mPagerAdapter);

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
